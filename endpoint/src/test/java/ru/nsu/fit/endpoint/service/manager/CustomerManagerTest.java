package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.UUID;

public class CustomerManagerTest {
    private DBService dbService;
    private Logger logger;
    private CustomerManager customerManager;

    private Customer customerBeforeCreateMethod;
    private Customer customerAfterCreateMethod;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        customerBeforeCreateMethod = new Customer()
                .setId(null)
                .setFirstName("John")
                .setLastName("Wick")
                .setLogin("john_wick@gmail.com")
                .setPass("Baba_Jaga")
                .setBalance(0);
        customerAfterCreateMethod = customerBeforeCreateMethod.clone();
        customerAfterCreateMethod.setId(UUID.randomUUID());

        Mockito.when(dbService.createCustomer(customerBeforeCreateMethod)).thenReturn(customerAfterCreateMethod);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
    }

    @Test
    public void testCreateNewCustomer() {
        // Вызываем метод, который хотим протестировать
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);

        // Проверяем результат выполенния метода
        Assert.assertEquals(customer.getId(), customerAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testCreateCustomerWithNullArgument() {
        try {
            customerManager.createCustomer(null);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Argument 'customerData' is null.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithEasyPassword() {
        try {
            customerBeforeCreateMethod.setPass("123qwe");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password is easy.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithEasyPassword1() {
        try {
            customerBeforeCreateMethod.setPass("1q2w3e");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password is easy.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithShortFirstName(){
        try{
            customerBeforeCreateMethod.setFirstName("A");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("FirstName's length should be more or equal 2 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithLongFirstName(){
        try{
            customerBeforeCreateMethod.setFirstName("Abbbbbbbbbbbbbbbbbbbbbb");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("FirstName's length should be more or equal 2 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithLowerCaseFirstLetterInFirstName(){
        try{
            customerBeforeCreateMethod.setFirstName("abbb");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("FirstName should start with uppercase letter.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithUpperCaseNotFirstLetterInFirstName(){
        try{
            customerBeforeCreateMethod.setFirstName("ABbbbb");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("Only first letter in FirstName should be uppercase.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithDigitInFirstName(){
        try{
            customerBeforeCreateMethod.setFirstName("Abbb1");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("FirstName should contain only letters.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithOtherSymbolInFirstName(){
        try{
            customerBeforeCreateMethod.setFirstName("Abbb@b");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("FirstName should contain only letters.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithShortLastName(){
        try{
            customerBeforeCreateMethod.setLastName("A");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("LastName's length should be more or equal 2 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithLongLastName(){
        try{
            customerBeforeCreateMethod.setLastName("Abbbbbbbbbbbbbbbbbbbbbb");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("LastName's length should be more or equal 2 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithLowerCaseFirstLetterInLastName(){
        try{
            customerBeforeCreateMethod.setLastName("abbb");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("LastName should start with uppercase letter.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithUpperCaseNotFirstLetterInLastName(){
        try{
            customerBeforeCreateMethod.setLastName("ABbbbb");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("Only first letter in LastName should be uppercase.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithDigitInLastName(){
        try{
            customerBeforeCreateMethod.setLastName("Abbb1");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("LastName should contain only letters.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithOtherSymbolInLastName(){
        try{
            customerBeforeCreateMethod.setLastName("Abbb@b");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("LastName should contain only letters.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithIncorrectEmail(){
        try {
            customerBeforeCreateMethod.setLogin("abc");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch(IllegalArgumentException ex) {
            Assert.assertEquals("Login should be correct.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomWithShortPass(){
        try {
            customerBeforeCreateMethod.setPass("1a2bc");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomWithLongPass(){
        try {
            customerBeforeCreateMethod.setPass("1sda2346fsdsdhdg6dfghgjsggdj87");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex){
            Assert.assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomWithEqualLoginAndPass(){
        try{
            customerBeforeCreateMethod.setPass("bc@gmail.com");
            customerBeforeCreateMethod.setLogin("c@gmail.com");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex){
            Assert.assertEquals("Password shouldn't contain login.", ex.getMessage());
        }

    }

    @Test
    public void testCreateCustomWithEqualFirstNameAndPass(){
        try{
            customerBeforeCreateMethod.setPass("Ekaterina123");
            customerBeforeCreateMethod.setFirstName("Ekaterina");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex){
            Assert.assertEquals("Password shouldn't contain FirstName.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomWithEqualLastNameAndPass(){
        try {
            customerBeforeCreateMethod.setPass("Bykova12345");
            customerBeforeCreateMethod.setLastName("Bykova");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex){
            Assert.assertEquals("Password shouldn't contain LastName.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomWithNonZeroBalance (){
        try {
            customerBeforeCreateMethod.setBalance(2);
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Balance should be equal 0.", ex.getMessage());
        }
    }
}
