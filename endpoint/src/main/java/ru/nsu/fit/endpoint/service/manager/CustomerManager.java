package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.List;
import java.lang.String;
import java.util.UUID;

public class CustomerManager extends ParentManager {
    public CustomerManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Customer. Ограничения:
     * Аргумент 'customerData' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * money - должно быть равно 0.
     */

    private boolean CheckLogin(String login){
        if (login.length()<11)
            return false;
        String res="@gmail.com";
        if (!res.equals(login.substring(login.length()-10, login.length())))
            return false;
        for (int i=0; login.charAt(i)!='@'; i++) {
            if (!(login.charAt(i) >= 'a' && login.charAt(i) <= 'z' || i > 0 && (login.charAt(i) == '.' || login.charAt(i) == '_' || login.charAt(i) == '-' || login.charAt(i) >= '0' && login.charAt(i) <= '9')))
                return false;
        }
        return true;
    }


    public Customer createCustomer(Customer customer) {
        Validate.notNull(customer, "Argument 'customerData' is null.");
        Validate.notNull(customer.getPass());
        Validate.isTrue(customer.getPass().length() >= 6 && customer.getPass().length() < 13, "Password's length should be more or equal 6 symbols and less or equal 12 symbols.");
        Validate.isTrue(!customer.getPass().equalsIgnoreCase("123qwe"), "Password is easy.");

        Validate.isTrue(!customer.getPass().equalsIgnoreCase("1q2w3e"), "Password is easy.");

        Validate.isTrue(customer.getFirstName().length()>=2 && customer.getFirstName().length()<=12, "FirstName's length should be more or equal 2 symbols and less or equal 12 symbols.");

        Validate.isTrue(customer.getFirstName().charAt(0)>='A' && customer.getFirstName().charAt(0)<='Z', "FirstName should start with uppercase letter.");

        boolean flag=false;
        String fName=customer.getFirstName();
        for(int i=1; i<fName.length(); i++)
            if(fName.charAt(i)>='A' && fName.charAt(i)<='Z')
                flag=true;
        Validate.isTrue(!flag, "Only first letter in FirstName should be uppercase.");

        for(int i=2; i<fName.length(); i++)
            if(!(fName.charAt(i)>='a' && fName.charAt(i)<='z'))
                flag=true;
        Validate.isTrue(!flag, "FirstName should contain only letters.");

        Validate.isTrue(customer.getLastName().length()>=2 && customer.getLastName().length()<=12, "LastName's length should be more or equal 2 symbols and less or equal 12 symbols.");

        Validate.isTrue(customer.getLastName().charAt(0)>='A' && customer.getLastName().charAt(0)<='Z', "LastName should start with uppercase letter.");

        String lName=customer.getLastName();
        for(int i=1; i<lName.length(); i++)
            if(lName.charAt(i)>='A' && lName.charAt(i)<='Z')
                flag=true;
        Validate.isTrue(!flag, "Only first letter in LastName should be uppercase.");

        for(int i=2; i<lName.length(); i++)
            if(!(lName.charAt(i)>='a' && lName.charAt(i)<='z'))
                flag=true;
        Validate.isTrue(!flag, "LastName should contain only letters.");

        Validate.isTrue(CheckLogin(customer.getLogin())==true, "Login should be correct.");

        Validate.isTrue(customer.getPass().lastIndexOf(customer.getLogin())<0, "Password shouldn't contain login.");

        Validate.isTrue(customer.getPass().lastIndexOf(customer.getFirstName())<0, "Password shouldn't contain FirstName.");

        Validate.isTrue(customer.getPass().lastIndexOf(customer.getLastName())<0, "Password shouldn't contain LastName.");

        Validate.isTrue(customer.getBalance()==0, "Balance should be equal 0.");

        return dbService.createCustomer(customer);
    }

    /**
     * Метод возвращает список объектов типа customer.
     */
    public List<Customer> getCustomers() {
        return dbService.getCustomers();
    }


    /**
     * Метод обновляет объект типа Customer.
     * Можно обновить только firstName и lastName.
     */
    public Customer updateCustomer(Customer customer) {
        throw new NotImplementedException("Please implement the method.");
    }

    public void removeCustomer(UUID id) {
        throw new NotImplementedException("Please implement the method.");
    }

    /**
     * Метод добавляет к текущему баласу amount.
     * amount - должен быть строго больше нуля.
     */
    public Customer topUpBalance(UUID customerId, int amount) {
        throw new NotImplementedException("Please implement the method.");
    }
}
